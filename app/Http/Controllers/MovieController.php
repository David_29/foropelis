<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use App\Models\Movie;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Image;

class MovieController extends Controller
{
    public function index()
    {
        //print_r(Auth::user());
        return view('pages.addMovie');
    }

    public function sendMovies(){
        //$moviesAllCategories=Movie::all();
        $movieslatest = Movie::orderBy('created_at','desc')->take(15)->get();
        $moviesAllCategories = Movie::inRandomOrder()->take(15)->get();
        return view('home.index')->with('moviesAllCategories',$moviesAllCategories)->with('movieslatest',$movieslatest);
    }

    public function sendMoviesSelection(Request $request){
        $category = $request->route('cat');
        if($category == "Tendencia"){
            $moviesSelection = Movie::orderBy('total_evaluations','desc')->get();
        }elseif ($category == "Mejor valorados"){
            $moviesSelection = Movie::orderBy('evaluation','desc')->orderBy('total_evaluations','desc')->get();
        }else{
            $moviesSelection = Movie::where("genre","=",$category)->orderBy('evaluation','desc')->get();
        }
        return view('pages.selectionMovies')->with('moviesSelection',$moviesSelection)->with('category',$category);
    }

    public function movieView(Request $request)
    {
        $id = $request->route('id');
        $movie = Movie::findOrFail($id);
        $comments_of_movie = DB::table('comments')
            ->join('users', 'comments.id_user', '=', 'users.id')
            ->select('comments.*', 'users.user_image','users.name')->where('comments.id_movie','=',$id)->get();
        if (Auth::check()){
            //Esto obtiene si el usuario en cuestion ha realizado comentarios sobre la pelicula en cuestion
            $comments_user_this_movie = Comment::where('id_user', "=", Auth::user()->id)->where('id_movie', "=", $id)->get();
            //Si no ha realizado comentarios, le saldrá la caja para realizar un comentario, si ya tiene un comentario, no le saldrá
            //la caja para comentar
            if($comments_user_this_movie->isEmpty()){
                return view('pages.movieView')->with('movie',$movie)->with("comments_before","empty")->with("comments_of_movie",$comments_of_movie);
            }else{
                return view('pages.movieView')->with('movie',$movie)->with("comments_before","fill")->with("comments_of_movie",$comments_of_movie);
            }
        }else{
            return view('pages.movieView')->with('movie',$movie)->with("comments_of_movie",$comments_of_movie);
        }
    }

    //Adition movie
    public function receiveForm(Request $request)
    {
        $movie=new Movie();
        $movie->name=$request->input('movieName');
        $movie->genre=$request->input('movieGenre');
        $movie->country=$request->input('movieCountry');
        $movie->year=$request->input('movieAnno');
        $movie->duration=$request->input('movieDuration');
        $movie->distribution=$request->input('movieDistribution');
        $movie->synopsis=$request->input('movieSynopsis');

        if ($request->hasfile('moviePhoto')) {
            $avatar = $request->file('moviePhoto');
            $filename = time() . '.' . $avatar->getClientOriginalExtension();

            //Implement check here to create directory if not exist already
            $movie->movie_image="uploads/imgs/".$filename;
            Image::make($avatar)->save(public_path('uploads/imgs/' . $filename));
        }
        $movie->save();
        return redirect('/');
    }
    //Edit movie
    public function editMovieView(Request $request)
    {
        $id_movie = $request->route('id');
        $movie=Movie::findOrFail($id_movie);
        return view('pages.editMovie')->with('movie',$movie);
    }
    //Remove movie
    public function removeMovie(Request $request)
    {
        $id_movie = $request->route('id');
        $movie=Movie::findOrFail($id_movie);
        $movie->delete();
        return redirect('/');
    }
    public function editMovieForm(Request $request)
    {
        $id_movie = $request->route('id');
        $movie=Movie::findOrFail($id_movie);
        $movie->name=$request->input('movieName');
        $movie->genre=$request->input('movieGenre');
        $movie->country=$request->input('movieCountry');
        $movie->year=$request->input('movieAnno');
        $movie->duration=$request->input('movieDuration');
        $movie->distribution=$request->input('movieDistribution');
        $movie->synopsis=$request->input('movieSynopsis');

        if ($request->hasfile('moviePhoto')) {
            $avatar = $request->file('moviePhoto');
            $filename = time() . '.' . $avatar->getClientOriginalExtension();

            //Implement check here to create directory if not exist already
            $movie->movie_image="uploads/imgs/".$filename;
            Image::make($avatar)->save(public_path('uploads/imgs/' . $filename));
        }
        $movie->save();
        return redirect('/movie/'.$id_movie);
    }
    //Adition comment
    public function receiveComment(Request $request)
    {
        $id_movie = $request->route('id');
        if (Auth::check()) {
            //Adición del comentario
            $comment = new Comment();
            $id_usuario = Auth::user()->id;
            $date = now()->addHours(2)->format('d-m-Y H:i:s');
            $comment_text = $request->input('comment');
            $rating_movie = $request->input('rate');

            $comment->id_user = $id_usuario;
            $comment->id_movie = $id_movie;
            $comment->date = $date;
            $comment->comment = $comment_text;
            $comment->valoration_comment = $rating_movie;
            $comment->save();
            //Gestión estadísticas de la película
            $movie_in_question = Movie::findOrFail($id_movie);
            $movie_in_question->number_of_evaluations+=$rating_movie;
            $movie_in_question->total_evaluations+=1;
            $movie_in_question->evaluation=$movie_in_question->number_of_evaluations/$movie_in_question->total_evaluations;
            $movie_in_question->save();
            return redirect('/movie/'.$id_movie);
        }else{
            return redirect('/movie/'.$id_movie)->with("notification","value");
        }

    }
    //Delete comment
    public function deleteComment(Request $request)
    {
        $id_movie = $request->route('id');
        $id_comment_eliminate=$request->route('id_comment');
        $comment=Comment::findOrFail($id_comment_eliminate);
        $movie=Movie::findOrFail($id_movie);
        $movie->total_evaluations-=1;
        $movie->number_of_evaluations=$movie->number_of_evaluations-$comment->valoration_comment;
        if($movie->total_evaluations!=0){
            $movie->evaluation=$movie->number_of_evaluations/$movie->total_evaluations;
        }else{
            $movie->evaluation=0;
        }

        $movie->save();
        $comment->delete();
        return redirect('/movie/'.$id_movie);
    }
    //Edit profile
    public function profileView(Request $request)
    {
        $id_user = $request->route('id');
        $user=User::findOrFail($id_user);
        $comments = Comment::where('id_user', '=', $id_user)->get();
        $comments_number = $comments->count();
        return view('pages.profileView')->with('user',$user)->with('comments_number',$comments_number);
    }
    public function receiveEditProfileForm(Request $request)
    {
        $id_user = $request->route('id');
        $user=User::findOrFail($id_user);
        if ($request->hasfile('userPhoto')) {
            echo "lleno";
        }
        if($request->input('passActual')!=""){
            $validacion =request()->validate([
                'passActual'=>[
                    function($attribute,$value,$fail){
                        if(!(Hash::check($value,Auth()->user()->password))){
                            $fail('La contraseña del usuario no coincide, no puede cambiar la contraseña.');
                        }
                    }
                ],
            ]);
            $user->name=$request->input('profileName');
            $user->email=$request->input('profileEmail');
            $user->password=Hash::make($request->input('profileNewPass'));
            if ($request->hasfile('userPhoto')) {
                $avatar = $request->file('userPhoto');
                $filename = time() . '.' . $avatar->getClientOriginalExtension();

                //Implement check here to create directory if not exist already
                $user->user_image=$filename;
                Image::make($avatar)->save(public_path('uploads/avatars/' . $filename));
            }
            $user->bio=$request->input('bio');
            $user->save();
            return redirect('/');
        }else{
            $user->name=$request->input('profileName');
            $user->email=$request->input('profileEmail');
            if ($request->hasfile('userPhoto')) {
                $avatar = $request->file('userPhoto');
                $filename = time() . '.' . $avatar->getClientOriginalExtension();
                //Implement check here to create directory if not exist already
                $user->user_image=$filename;
                Image::make($avatar)->save(public_path('uploads/avatars/' . $filename));
            }
            $user->bio=$request->input('bio');
            $user->save();
            return redirect('/');
        }
    }
    //Visit other profile
    public function visitingProfile(Request $request)
    {
        $id_user = $request->route('id');
        $user=User::findOrFail($id_user);
        $comments = Comment::where('id_user', '=', $id_user)->get();
        $comments_number = $comments->count();
        return view('pages.foreignProfile')->with('user',$user)->with('comments_number',$comments_number);
    }
}
