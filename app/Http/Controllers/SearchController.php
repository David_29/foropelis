<?php

namespace App\Http\Controllers;

use App\Models\Movie;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function searching(Request $request)
    {
        /*$movies=Movie::where('name','like',$request->texto."%")->take(5)->get();
        return view("home.index", compact("searchingMovies"));*/
        $term= $request->get('term');
        $movies=Movie::where('name','like','%'.$term.'%')->get();
        $data=[];
        foreach ($movies as $movie){
            $distribution=$movie->distribution;
            $names=explode(",",$distribution);
            $dates_of_actors=$names[0].", ".$names[1].", ".$names[2].".";
            $data[]=[
                "name" => $movie->name,
                "id" => $movie->id,
                "photo" => $movie->movie_image,
                "year" => $movie->year,
                "distribution" => $dates_of_actors,
                "genre" => $movie->genre
            ];
        }
        return json_encode($data);
    }
}
