<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use HasFactory;

    public function toUsers(){
        return $this->belongsTo(User::class);
    }

    public function toMovies(){
        return $this->belongsTo(Movie::class);
    }
}
