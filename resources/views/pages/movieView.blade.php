@extends('layouts.master')
@section('title','Vista de película')

@section('content')
    <div class="row align-items-center mt-5">
        <div class="col-md-auto col-12">
            <figure class="w-75 mx-auto my-auto"><img class="img-fluid maximum-width-photo-view" src="{{asset($movie->movie_image)}}"></figure>
        </div>
        <div class="col mt-3">
            <div class="row justify-content-md-start justify-content-center">
                <div class="col-auto">
                    <h1><b>{{$movie->name}}</b></h1>
                </div>
            </div>
            <div class="row justify-content-md-start justify-content-center mt-3">
                <div class="col-auto border-custom">
                    {{$movie->duration}} min.
                </div>
                <div class="col-auto border-custom">
                    {{$movie->genre}}
                </div>
                <div class="col-auto border-custom">
                    {{$movie->country}}
                </div>
                <div class="col-auto">
                    {{$movie->year}}
                </div>
            </div>
            <div class="row justify-content-md-start mt-3 justify-content-center">
                <div class="col-auto">
                    <b>Reparto:</b><br> {{$movie->distribution}}.
                </div>
            </div>
            <div class="row justify-content-md-start justify-content-center mt-3">
                <div class="col overflow-auto">
                    <b>Sinopsis:</b><textarea class="textarea-styles" rows="6" readonly>{{$movie->synopsis}}</textarea>
                </div>
            </div>
        </div>
    </div>
    <div class="row justify-content-between align-items-start mt-1">
        <div class="col-auto">
            Valoración usuarios:
        </div>
        @if(Auth::check())
            @if(Auth()->User()->role=="Administrador")
                <div class="col-auto">
                    <button class="btn btn-info text-white" name="delete-comment">
                        <a class="text-white text-decoration-none" href="{{url('/editMovie/'.$movie->id)}}">Editar película</a>
                    </button>
                </div>
            @endif
        @endif
    </div>
    <div class="row justify-content-between align-items-center no-gutters mt-2">
        <div class="col-auto">
            <div class="row justify-content-start align-items-center no-gutters">
                <div class="col-auto graphic-valoration">
                    <div class="star_content">
                        @if($movie->evaluation==1)
                            <input type="radio" class="star2" disabled checked="checked"/>
                            <input name="rate" value="2" type="radio" class="star2"/>
                            <input name="rate" value="3" type="radio" class="star2"/>
                            <input name="rate" value="4" type="radio" class="star2" />
                            <input name="rate" value="5" type="radio" class="star2"/>
                        @endif
                        @if($movie->evaluation==2)
                            <input type="radio" class="star2" disabled />
                            <input name="rate" value="2" type="radio" class="star2" checked="checked"/>
                            <input name="rate" value="3" type="radio" class="star2"/>
                            <input name="rate" value="4" type="radio" class="star2"/>
                            <input name="rate" value="5" type="radio" class="star2"/>
                        @endif
                        @if($movie->evaluation==3)
                            <input type="radio" class="star2" disabled />
                            <input name="rate" value="2" type="radio" class="star2"/>
                            <input name="rate" value="3" type="radio" class="star2" checked="checked"/>
                            <input name="rate" value="4" type="radio" class="star2"/>
                            <input name="rate" value="5" type="radio" class="star2"/>
                        @endif
                        @if($movie->evaluation==4)
                            <input type="radio" class="star2" disabled />
                            <input name="rate" value="2" type="radio" class="star2"/>
                            <input name="rate" value="3" type="radio" class="star2"/>
                            <input name="rate" value="4" type="radio" class="star2" checked="checked"/>
                            <input name="rate" value="5" type="radio" class="star2"/>
                        @endif
                        @if($movie->evaluation==5)
                            <input type="radio" class="star2" disabled />
                            <input name="rate" value="2" type="radio" class="star2"/>
                            <input name="rate" value="3" type="radio" class="star2"/>
                            <input name="rate" value="4" type="radio" class="star2"/>
                            <input name="rate" value="5" type="radio" class="star2" checked="checked"/>
                        @endif
                    </div>
                </div>
                <div class="col-auto">
                    <span class="text-number-valorations">con @if($movie->total_evaluations==0) {{0}} @else {{$movie->total_evaluations}} @endif valoraciones</span>
                </div>
            </div>
        </div>
        @if(Auth::check())
            @if(Auth()->User()->role=="Administrador")
                <div class="col-auto">
                    <button class="btn btn-warning text-white" name="delete-comment">
                        <a class="text-white text-decoration-none" href="{{url('/removeMovie/'.$movie->id)}}">Eliminar película</a>
                    </button>
                </div>
            @endif
        @endif
    </div>

    <hr class="bg-light">
    @if(Auth::check())
        @if($comments_before=="empty")
            <div class="container mb-5">
                <div class="row justify-content-center">
                    <div class="col-8">
                        <form class="mx-auto" action="" method="POST">
                            @csrf
                            <div class="row justify-content-center">
                                <div class="col">
                                    <textarea placeholder="Escriba aquí su reseña" class="styles-box-comment w-100" name="comment" required></textarea>
                                </div>
                            </div>
                            <div class="row justify-content-between align-items-center">
                                <div class="col-auto">
                                    <button class="btn btn-success" type="submit" name="send-comment">Enviar reseña</button>
                                </div>
                                <div class="col-auto">
                                    <div class="row justify-content-md-end no-gutters align-items-center">
                                        <div class="col-auto mr-2">
                                            Valoración:
                                        </div>
                                        <div class="col-auto">
                                            <div class="star_content">
                                                <input name="rate" value="1" type="radio" class="star"/>
                                                <input name="rate" value="2" type="radio" class="star"/>
                                                <input name="rate" value="3" type="radio" class="star"/>
                                                <input name="rate" value="4" type="radio" class="star" checked="checked"/>
                                                <input name="rate" value="5" type="radio" class="star"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        @endif

    @else
        <div class="container mb-5">
            <div class="row justify-content-center">
                <div class="col-8">
                    <form class="mx-auto" action="" method="POST">
                        @csrf
                        <div class="row justify-content-center">
                            <div class="col">
                                <textarea placeholder="Escriba aquí su reseña" class="styles-box-comment w-100" name="comment" required></textarea>
                            </div>
                        </div>
                        <div class="row justify-content-between align-items-center">
                            <div class="col-auto">
                                <button class="btn btn-success" type="submit" name="send-comment">Enviar reseña</button>
                            </div>
                            <div class="col-auto">
                                <div class="row justify-content-md-end no-gutters align-items-center">
                                    <div class="col-auto mr-2">
                                        Valoración:
                                    </div>
                                    <div class="col-auto">
                                        <div class="star_content">
                                            <input name="rate" value="1" type="radio" class="star"/>
                                            <input name="rate" value="2" type="radio" class="star"/>
                                            <input name="rate" value="3" type="radio" class="star"/>
                                            <input name="rate" value="4" type="radio" class="star" checked="checked"/>
                                            <input name="rate" value="5" type="radio" class="star"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
            <div class="row justify-content-center mt-3">
                <div class="style-warning-comment">Debe iniciar sesión para poder enviar un comentario</div>
                </div>
            </div>
        </div>

    @endif
    <hr class="bg-light">
    @foreach($comments_of_movie as $value)
        @if(Auth::check())
            @if($value->id_user == Auth::user()->id)
                <div class="container style-box-comment-core w-75 mt-5 comment-movie mb-5">
                    <div class="row justify-content-between pt-3 align-items-center">
                        <div class="col-auto">
                            <div class="row justify-content-start align-items-center user-commt-info">
                                <div class="col-auto">
                                    <a class="" href="{{asset('/profileView/'.Auth()->user()->id)}}">
                                        <div ><figure class="mb-0 size-photo-user-comment"><img class="img-fluid" src="{{asset("/uploads/avatars/".$value->user_image)}}"></figure></div>
                                    </a>
                                </div>
                                <div class="col-auto">
                                    <div class="row justify-content-center align-items-center">
                                        <div class="col-12">
                                            <a class="" href="{{asset('/profileView/'.Auth()->user()->id)}}">
                                                <b>{{$value->name}}</b>
                                            </a>
                                        </div>
                                        <div class="col-12">
                                            <a class="" href="{{asset('/profileView/'.Auth()->user()->id)}}">
                                                <span class="position-relative hour-comment">{{$value->date}}</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="row justify-content-between">
                                <div class="col-auto">
                                    <div class="star_content mt-2">
                                        @if($value->valoration_comment==1)
                                            <figure class="size-valoration-box-comment m-0"><img class="img-fluid" src="{{asset("/stars/1.PNG")}}"> </figure>
                                        @endif
                                        @if($value->valoration_comment==2)
                                            <figure class="size-valoration-box-comment m-0"><img class="img-fluid" src="{{asset("/stars/2.PNG")}}"> </figure>
                                        @endif
                                        @if($value->valoration_comment==3)
                                            <figure class="size-valoration-box-comment m-0"><img class="img-fluid" src="{{asset("/stars/3.PNG")}}"> </figure>
                                        @endif
                                        @if($value->valoration_comment==4)
                                            <figure class="size-valoration-box-comment m-0"><img class="img-fluid" src="{{asset("/stars/4.PNG")}}"> </figure>
                                        @endif
                                        @if($value->valoration_comment==5)
                                            <figure class="size-valoration-box-comment m-0"><img class="img-fluid" src="{{asset("/stars/5.PNG")}}"> </figure>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-auto">
                                    <button class="btn btn-danger text-white" name="delete-comment">
                                        <a class="text-white text-decoration-none" href="{{url('/movie/delete_comment/'.$movie->id.'/'.$value->id)}}">Eliminar comentario</a>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-between align-items-center mt-3">
                        <div class="col-12">
                            <p>{{$value->comment}}</p>
                        </div>
                    </div>
                </div>
            @else
                <div class="container w-75 mt-5 comment-movie mb-5">
                    <div class="row justify-content-between pt-3 align-items-center">
                        <div class="col-auto">
                            <div class="row justify-content-start align-items-center user-commt-info">
                                <div class="col-auto">
                                    <a class="" href="{{asset('/profileViewVisiting/'.$value->id_user)}}">
                                        <div>
                                            <figure class="mb-0 size-photo-user-comment">
                                                <img class="img-fluid" src="{{asset("/uploads/avatars/".$value->user_image)}}">
                                            </figure>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-auto">
                                    <div class="row justify-content-center align-items-center">
                                        <div class="col-12">
                                            <a class="" href="{{asset('/profileViewVisiting/'.$value->id_user)}}">
                                                <b>{{$value->name}}</b>
                                            </a>
                                        </div>
                                        <div class="col-12">
                                            <a class="" href="{{asset('/profileViewVisiting/'.$value->id_user)}}">
                                                <span class="position-relative hour-comment">{{$value->date}}</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-auto">
                            <div class="row">
                                <div class="col-auto">
                                    <div class="star_content mt-2">
                                        @if($value->valoration_comment==1)
                                            <figure class="size-valoration-box-comment m-0"><img class="img-fluid" src="{{asset("/stars/1.PNG")}}"> </figure>
                                        @endif
                                        @if($value->valoration_comment==2)
                                            <figure class="size-valoration-box-comment m-0"><img class="img-fluid" src="{{asset("/stars/2.PNG")}}"> </figure>
                                        @endif
                                        @if($value->valoration_comment==3)
                                            <figure class="size-valoration-box-comment m-0"><img class="img-fluid" src="{{asset("/stars/3.PNG")}}"> </figure>
                                        @endif
                                        @if($value->valoration_comment==4)
                                            <figure class="size-valoration-box-comment m-0"><img class="img-fluid" src="{{asset("/stars/4.PNG")}}"> </figure>
                                        @endif
                                        @if($value->valoration_comment==5)
                                            <figure class="size-valoration-box-comment m-0"><img class="img-fluid" src="{{asset("/stars/5.PNG")}}"> </figure>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-between align-items-center mt-3">
                        <div class="col-12">
                            <p>{{$value->comment}}</p>
                        </div>
                    </div>
                </div>
            @endif
        @else
            <div class="container w-50 mt-5 comment-movie mb-5">
                <div class="row justify-content-between pt-3 align-items-center">
                    <div class="col-auto">
                        <div class="row justify-content-start align-items-center">
                            <div class="col-auto">
                                <div ><figure class="mb-0 size-photo-user-comment"><img class="img-fluid" src="{{asset("/uploads/avatars/".$value->user_image)}}"></figure></div>
                            </div>
                            <div class="col-auto">
                                <div class="row justify-content-center align-items-center">
                                    <div class="col-12">
                                        <b>{{$value->name}}</b>
                                    </div>
                                    <div class="col-12">
                                        <span class="position-relative hour-comment">{{$value->date}}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-auto">
                        <div class="row mt-3 mt-lg-0">
                            <div class="col-auto">
                                <div class="star_content mt-2">
                                    @if($value->valoration_comment==1)
                                        <figure class="size-valoration-box-comment m-0"><img class="img-fluid" src="{{asset("/stars/1.PNG")}}"> </figure>
                                    @endif
                                    @if($value->valoration_comment==2)
                                        <figure class="size-valoration-box-comment m-0"><img class="img-fluid" src="{{asset("/stars/2.PNG")}}"> </figure>
                                    @endif
                                    @if($value->valoration_comment==3)
                                        <figure class="size-valoration-box-comment m-0"><img class="img-fluid" src="{{asset("/stars/3.PNG")}}"> </figure>
                                    @endif
                                    @if($value->valoration_comment==4)
                                        <figure class="size-valoration-box-comment m-0"><img class="img-fluid" src="{{asset("/stars/4.PNG")}}"> </figure>
                                    @endif
                                    @if($value->valoration_comment==5)
                                        <figure class="size-valoration-box-comment m-0"><img class="img-fluid" src="{{asset("/stars/5.PNG")}}"> </figure>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-between align-items-center mt-3">
                    <div class="col-12">
                        <p>{{$value->comment}}</p>
                    </div>
                </div>
            </div>
        @endif
    @endforeach

    <script>
        $(document).ready(function(){
            $('input.star').rating();
            $('input.star2').rating();
        });
    </script>
@endsection