@extends('layouts.master')
@section('title','Vista de perfil')

@section('content')
    <div class="bg-profile text-center pb-4 mt-5 w-75 mx-auto mobile-extended">
        <div class="row justify-content-around align-items-center">
            <div class="col-auto mt-4 user-photo-view">
                <figure><img src="{{asset("/uploads/avatars/".$user->user_image)}}" ></figure>
            </div>
            <div class="col-auto">
                <h1 class="py-5">Mi cuenta: <span class="font-weight-bold position-relative"><div class="box-shadow-cust-name"></div>{{$user->name}}</span></h1>
            </div>
        </div>
        <h4 class="pb-5">Email: <span class="font-weight-bold position-relative"><div class="box-shadow-cust-email"></div>{{$user->email}}</span></h4>
        <h6>Número de comentarios realizados:  <span class="font-weight-bold position-relative"><div class="box-shadow-cust-dates"></div>{{$comments_number}}</span></h6>
    </div>
    <div class="form-edit-profile w-75 mx-auto mt-4 pb-4 mobile-extended">
        <form method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group row">
                <label for="name" class="col-md-2 col-form-label text-md-center">Nickname:</label>
                <div class="col-md-4">
                    <input type="text" class="form-control" name="profileName" id="profileName" value="{{$user->name}}" placeholder="Nombre perfil" required>
                </div>
                <label for="name" class="col-md-2 col-form-label text-md-center">E-Mail:</label>
                <div class="col-md-4">
                    <input type="text" class="form-control" name="profileEmail" id="profileEmail" value="{{$user->email}}" placeholder="Email" required>
                </div>
            </div>
            <div class="form-group row">
                <label for="name" class="col-md-2 col-form-label text-md-center">Foto:</label>
                <div class="col-md-4">
                    <input id="userPhoto" type="file" class="form-control" name="userPhoto" value="{{ old('userPhoto') }}">
                </div>
                <label for="role" class="col-md-2 col-form-label text-md-center">Rol:</label><br>
                <div class="col-md-4">
                    <input id="role" type="text" class="form-control" name="role" value="{{$user->role}}" readonly>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-auto">
                    <div class="mx-auto">
                        <label class="text-center">*Rellene estos campos si desea cambiar la contraseña*</label>
                    </div>
                </div>
            </div>
            <div class="form-group row justify-content-center align-items-center">
                <label for="passActual" class="col-md-2 col-form-label text-md-center">Contraseña Actual</label>
                <div class="col-md-4">
                    <input type="password" class="form-control @error('passActual') is-invalid @enderror" id="passActual" name="passActual" placeholder="Contraseña anterior">
                    @error('passActual')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <label for="name" class="col-md-2 col-form-label text-md-center">Nueva contraseña:</label>
                <div class="col-md-4">
                    <input type="text" class="form-control" name="profileNewPass" id="profileNewPass" placeholder="Nueva contraseña">
                </div>
            </div>
            <div class="form-group row">
                <label for="name" class="col-md-2 col-form-label text-md-center">Biografía:</label>
                <div class="col">
                    <textarea name="bio" id="bio" class="marginSelectRegisterForm w-100 ml-0" rows="7" value="{{ old('bio') }}">{{$user->bio}}</textarea>
                </div>
            </div>
            <div class="form-group row mb-0">
                <div class="col-md-6 offset-md-2">
                    <button type="submit" class="btn btn-success">
                        {{ __('Guardar perfil') }}
                    </button>
                </div>
            </div>
        </form>
    </div>
@endsection