@extends('layouts.master')
@section('title','Vista de perfil')

@section('content')
    <div class="bg-profile text-center pb-4 mt-5 w-75 mx-auto mobile-extended">
        <div class="row justify-content-center align-items-center">
            <div class="col-12">
                <div class="row justify-content-around">
                    <div class="col-auto mt-4 user-photo-view">
                        <figure><img src="{{asset("/uploads/avatars/".$user->user_image)}}"></figure>
                    </div>
                    <div class="col-auto">
                        <h1 class="py-5">Usuario: <span class="font-weight-bold position-relative"><div class="box-shadow-cust-name"></div>{{$user->name}}</span></h1>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="row">
                    <div class="col-auto offset-md-1">
                        <h6>Número de comentarios realizados:  <span class="font-weight-bold position-relative"><div class="box-shadow-cust-dates"></div>{{$comments_number}}</span></h6>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="row">
                    <div class="col-auto offset-md-1">
                        <h6 class="text-left">Rol: {{$user->role}}</h6>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="row">
                    <div class="col-auto offset-md-1">
                        <h6 class="text-left">Biografía:</h6>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="row">
                    <div class="col-12">
                        <textarea name="bio" id="bio" class="marginSelectRegisterForm styles-bio-visiting w-75 ml-0" rows="4" value="{{ old('bio') }}" readonly>{{$user->bio}}</textarea>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection