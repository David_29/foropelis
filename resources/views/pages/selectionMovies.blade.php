@extends('layouts.master')
@section('title',$category)
@section('content')
    <h1 class="mt-5 mb-4 ml-4"><u><b>{{$category}}</b></u></h1>
    <div class="container-fluid">
        <div class="row justify-content-center align-items-center">
            @foreach($moviesSelection as $movie)
                <div class="col-7 col-md-4 col-lg-3">
                    <a class="linkMovie nonStyleLinks" href="{{url('/movie/'.$movie->id)}}">
                        <div class="row">
                            <div class="col-12">
                                <figure class="boxPhotoSelection">
                                    <img src="{{asset($movie->movie_image)}}">
                                </figure>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 pb-5">
                                <h5 class="text-center mt-n2 maximum-height-title">{{$movie->name}}</h5>
                            </div>
                        </div>
                    </a>
                </div>
            @endforeach
        </div>
    </div>
@endsection