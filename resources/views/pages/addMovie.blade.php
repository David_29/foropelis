@extends('layouts.master')
@section('title','Agregar película')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card bg-dark text-light">
                    <div class="card-header">Adición de película</div>

                    <div class="card-body">
                        <form method="POST" enctype="multipart/form-data">
                            @csrf

                            <div class="form-group row">
                                <label for="name" class="col-md-2 col-form-label text-md-center">Nombre:</label>

                                <div class="col-md-4">
                                    <input type="text" class="form-control" name="movieName" id="movieName" value="{{old('movieName')}}" placeholder="Nombre película" required>
                                </div>
                                <label for="name" class="col-md-2 col-form-label text-md-center">Género:</label>
                                <div class="col-md-4">
                                    <select class="form-control" name="movieGenre" id="movieGenre" value="{{old('movieGenre')}}" required>
                                        <!--<option>Tendencia</option>-->
                                        <option>Clásicos</option>
                                        <option>Acción</option>
                                        <option>Ciencia ficción</option>
                                        <option>Romántico</option>
                                        <option>Bélico</option>
                                        <option>Drama</option>
                                        <option>Comedia</option>
                                        <option>Terror</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-md-2 col-form-label text-md-center">Año:</label>
                                <div class="col-md-4">
                                    <input type="number" min="1900" max="2022" class="form-control" name="movieAnno" id="movieAnno" value="{{old('movieAnno')}}" placeholder="Año película" required>
                                </div>
                                <label for="name" class="col-md-2 col-form-label text-md-center">País/es:</label>
                                <div class="col-md-4">
                                    <input type="text" class="form-control" name="movieCountry" id="movieCountry" value="{{old('movieCountry')}}" placeholder="País/es película" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="duration" class="col-md-2 col-form-label text-md-center">Duración:</label>
                                <div class="col-md-4">
                                    <input type="number" min="30" max="500" class="form-control" name="movieDuration" id="movieDuration" value="{{old('movieDuration')}}" placeholder="Duración película" required>
                                </div>
                                <label for="distribution" class="col-md-2 col-form-label text-md-center">Reparto:</label>
                                <div class="col-md-4">
                                    <input type="text" class="form-control" name="movieDistribution" id="movieDistribution" value="{{old('movieDistribution')}}" placeholder="Reparto película" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-md-2 col-form-label text-md-center">Sinopsis:</label>
                                <div class="col-md-8">
                                    <textarea class="stylesTextarea" name="movieSynopsis" id="movieSynopsis" value="{{old('movieSynopsis')}}" placeholder="Sinopsis película" required rows="5" cols="95"></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="image" class="col-md-2 col-form-label text-md-right">Foto película</label><br>
                                <div class="col-md-5">
                                    <input id="moviePhoto" type="file" class="form-control" name="moviePhoto" value="{{ old('moviePhoto') }}" required>
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-2">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Agregar película') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
