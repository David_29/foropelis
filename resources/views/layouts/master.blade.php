<!doctype html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <link rel="stylesheet" type="text/css" href="{{asset('css/slick.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('css/slick-theme.css')}}"/>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <link rel="icon" sizes="192x192" href="{{asset("imgs/logo_chico.png")}}" />
    <link rel="apple-touch-icon" href="{{asset("imgs/logo_chico.png")}}" />
    <meta name="msapplication-TileImage" content="{{asset("imgs/logo_chico.png")}}" />
    <meta name="msapplication-square310x310logo" content="{{asset("imgs/logo_chico.png")}}" />
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/styles.css') }}" rel="stylesheet">
    <title>@yield('title')-ForoPelis</title>
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="{{url("/")}}"><figure class="m-0"><img class="img-fluid" src="{{asset("imgs/logo_grande.svg")}}"></figure></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Categorías
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="{{url('/selectionMovies/Tendencia')}}">Tendencia</a>
                        <a class="dropdown-item" href="{{url('/selectionMovies/Mejor valorados')}}">Mejor valorados</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="{{url('/selectionMovies/Clásicos')}}">Clásicos</a>
                        <a class="dropdown-item" href="{{url('/selectionMovies/Ciencia ficción')}}">Ciencia ficción</a>
                        <a class="dropdown-item" href="{{url('/selectionMovies/Acción')}}">Acción</a>
                        <a class="dropdown-item" href="{{url('/selectionMovies/Romántico')}}">Romántico</a>
                        <a class="dropdown-item" href="{{url('/selectionMovies/Bélico')}}">Bélico</a>
                        <a class="dropdown-item" href="{{url('/selectionMovies/Drama')}}">Drama</a>
                        <a class="dropdown-item" href="{{url('/selectionMovies/Comedia')}}">Comedia</a>
                        <a class="dropdown-item" href="{{url('/selectionMovies/Terror')}}">Terror</a>
                    </div>
                </li>
                <div class="form-inline my-2 my-lg-0 position-relative">
                    <input class="form-control mr-sm-2" id="text-searching" type="search" placeholder="Busca" aria-label="Search">
                    <div class="results-searching">

                    </div>
                    <!--<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Buscar</button>-->
                </div>
                @if(Auth::check() && Auth()->user()->role == "Administrador")
                    <li class="nav-item"><a class="nav-link" href="{{url('/addMovie')}}">Agregar película</a></li>
                @endif
            </ul>


            <!--GESTION BOTON INICIO DE SESION-->
            @if(Auth::check())
                <a class="text-light nonStyleLinks mr-5" href="{{asset('/profileView/'.Auth()->user()->id)}}" id="menuPerfil" role="button" aria-haspopup="true" aria-expanded="false">
                    {{Auth()->user()->name}}
                </a>
                <form action="{{url('logout')}}" class="form-inline justify-content-lg-center justify-content-sm-start" method="POST">
                    {{ csrf_field() }}
                    <a class="text-light nonStyleLinks">
                        <button class="btn btn-outline-light" type="submit">Cerrar Sesión</button>
                    </a>
                </form>
            @else
                <div class="form-inline my-2 my-lg-0">
                    <a href="{{url('home')}}"><button class="btn btn-outline-light mr-md-2 my-2 my-sm-0" type="submit">Iniciar Sesión</button></a>
                    <a href="{{url('register')}}"><button class="btn btn-dark btn-outline-light my-2 my-sm-0" type="submit">Registro</button></a>
                </div>
            @endif

            <!--<a href="{{url('home')}}"><button class="btn btn-dark btn-outline-light mr-md-2 my-2 my-sm-0" type="submit">Iniciar sesión</button></a>
            <a href="{{url('register')}}"><button class="btn btn-dark btn-outline-light my-2 my-sm-0" type="submit">Registro</button></a>-->

        </div>
    </nav>
    <div class="container">
        @yield('content')
    </div>
    <script src="{{asset("js/jquery-3.6.0.min.js")}}"></script>
    <script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="{{asset('js/slick.min.js')}}"></script>
    <script src={{asset("js/jquery.rating.pack.js")}}></script>
    <script src={{asset("js/jquery-ui.min.css")}}></script>
    <script src={{asset("js/jquery-ui.min.js")}}></script>
    <script src="{{asset("js/main.js")}}"></script>
</body>
</html>