@extends('layouts.master')
@section('title','Inicio')
@php
    use App\Models\Movie;
    if(!isset($moviesAllCategories)){
        $moviesAllCategories = Movie::inRandomOrder()->take(15)->get();
    }
    if(!isset($movieslatest)){
        $movieslatest = Movie::orderBy('created_at','desc')->take(15)->get();
    }
@endphp
@section('content')
    <h1 class="mt-5 mb-4 ml-4"><u><b>Todo cine</b></u></h1>
    <div class="container-fluid">
        <div class="slick-slider">
            @foreach($moviesAllCategories as $movie)
                <a class="linkMovie nonStyleLinks" href="{{url('/movie/'.$movie->id)}}">
                    <div class="">
                        <div class="row">
                            <div class="col-12">
                                <div class="banner-slide position-relative">
                                    <img src="{{asset($movie->movie_image)}}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 pb-5">
                                <h5 class="text-center mt-2 maximum-height-title">{{$movie->name}}</h5>
                            </div>
                        </div>
                    </div>
                </a>
            @endforeach
        </div>
    </div>
    <h1 class="mt-5 mb-4 ml-4"><u><b>Últimas películas</b></u></h1>
    <div class="container-fluid">
        <div class="slick-slider">
            @foreach($movieslatest as $movie)
                <a class="linkMovie nonStyleLinks" href="{{url('/movie/'.$movie->id)}}">
                    <div class="">
                        <div class="row">
                            <div class="col-12">
                                <div class="banner-slide position-relative">
                                    <img src="{{asset($movie->movie_image)}}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 pb-5">
                                <h5 class="text-center mt-2 maximum-height-title">{{$movie->name}}</h5>
                            </div>
                        </div>
                    </div>
                </a>
            @endforeach
        </div>
    </div>
@endsection
@section('import-js')
    <script src="{{asset("js/main.js")}}">
        //console.log("Hola mundo")
    </script>

@endsection