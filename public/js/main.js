$(function(){
    var slickOptions = {
        infinite: true,
        autoplay: false,
        slidesToShow: 6,
        slidesToScroll: 6,
        arrows: false,
        dots: true,
        autoplaySpeed: 4000,
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 4,
                }
            },
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                }
            },
            {
                breakpoint: 576,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                }
            },
            {
                breakpoint: 400,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                }
            },
        ]
    }
    $('.slick-slider').slick(slickOptions)
});

$( document ).ready(function() {
    $( "#text-searching" ).keyup(function() {
        var pathname = location.pathname;
        var hostname=location.hostname;
        //si indexOf devuelve -1 es que ese substring no está en el string
        //var num=url.indexOf("movie");
        //console.log(location)
        //dependiendo del path, cargo la ruta /buscador o la ruta /foroPelis/public/buscador para encontrar el controlador
        if(pathname=='/'){
            $.ajax({
                url: "/buscador",
                dataType: 'json',
                data: {
                    term: $("#text-searching").val()
                },
                success: function (data) {
                    //console.log(data)
                    if($("#text-searching").val()==""){
                        $(".results-searching").fadeOut("fast");
                    }else{
                        //se mandan los datos al contenedor para su visualización
                        //comprobamos la ruta en la que estamos para que las fotos carguen bien ya que no podemos usar el asset
                        $(".results-searching").html(build_list(data,pathname,hostname))
                        //emerge el panel
                        $(".results-searching").fadeIn("fast");
                        //contenedor con la info
                        var containerInfo = $(".results-searching");
                        //caja de búsqueda
                        var boxSearch = $("#text-searching");
                        //enlace que lleva a la película
                        var linked=$(".linked");
                        $( "body" ).click(function(e) {
                            if (!containerInfo.is(e.target) && $(e.target).closest(containerInfo).length == 0){
                                $(".results-searching").fadeOut("fast");
                            }
                            if (boxSearch.is(e.target)){
                                $(".results-searching").fadeIn("fast");
                            }
                            //console.log(e.target)
                        });
                    }

                    //response(data)
                }
            });
        }else{
            $.ajax({
                url: "/foroPelis/public/buscador",
                dataType: 'json',
                data: {
                    term: $("#text-searching").val()
                },
                success: function (data) {
                    console.log(data)
                    if($("#text-searching").val()==""){
                        $(".results-searching").fadeOut("fast");
                    }else{
                        //se mandan los datos al contenedor para su visualización
                        //comprobamos la ruta en la que estamos para que las fotos carguen bien ya que no podemos usar el asset
                        $(".results-searching").html(build_list(data,pathname,hostname))
                        //emerge el panel
                        $(".results-searching").fadeIn("fast");
                        //contenedor con la info
                        var containerInfo = $(".results-searching");
                        //caja de búsqueda
                        var boxSearch = $("#text-searching");
                        //enlace que lleva a la película
                        var linked=$(".linked");
                        $( "body" ).click(function(e) {
                            if (!containerInfo.is(e.target) && $(e.target).closest(containerInfo).length == 0){
                                $(".results-searching").fadeOut("fast");
                            }
                            if (boxSearch.is(e.target)){
                                $(".results-searching").fadeIn("fast");
                            }
                            //console.log(e.target)
                        });
                    }

                    //response(data)
                }
            });
        }

    });
    function build_list(data,path,hostname) {
        let html_text="";
        for(let i = 0;i < data.length;i++){
            //comprobamos si la ruta es la raiz o no, para cargar las fotos adecuadamente ya que no podemos usar asset
            //cambiamos redirección y ruta de foto según si es local y en que ruta
            if(path=='/'){
                html_text+= "<a class='linked' href='/movie/"+data[i].id+"'>" +
                    "<div class='row align-items-center no-gutters text-dark'>" +
                    "<div class='col-5 ml-1 mt-3'>" +
                    "<div class='photo-result-movie'><figure><img src='"+data[i].photo+"'></figure></div>" +
                    "</div>" +
                    "<div class='col ml-1'>" +
                    "<div class='row mx-0 justify-content-center align-items-center'>" +
                    "<div class='col-12'>Género: "+data[i].genre+"</div> " +
                    "<div class='col-12'>" +
                    "<div class='name-result-movie font-weight-bold py-2'>"+data[i].name+" ("+data[i].year+")</div>" +
                    "</div>" +
                    "<div class='col-12'>" +
                    "<div class='distribution'><i>"+data[i].distribution+"</i></div>" +
                    "</div>" +
                    "</div>" +
                    "</div>" +
                    "</div>" +
                    "</a>" +
                    "<hr>";
            }else{
                //RUTAS DEL LOCAL
                if(path=='/foroPelis/public/'){
                    html_text+= "<a class='linked' href='/foroPelis/public/movie/"+data[i].id+"'>" +
                        "<div class='row align-items-center no-gutters text-dark'>" +
                        "<div class='col-5 ml-1 mt-3'>" +
                        "<div class='photo-result-movie'><figure><img src='"+data[i].photo+"'></figure></div>" +
                        "</div>" +
                        "<div class='col ml-1'>" +
                        "<div class='row mx-0 justify-content-center align-items-center'>" +
                        "<div class='col-12'>Género: "+data[i].genre+"</div> " +
                        "<div class='col-12'>" +
                        "<div class='name-result-movie font-weight-bold py-2'>"+data[i].name+" ("+data[i].year+")</div>" +
                        "</div>" +
                        "<div class='col-12'>" +
                        "<div class='distribution'><i>"+data[i].distribution+"</i></div>" +
                        "</div>" +
                        "</div>" +
                        "</div>" +
                        "</div>" +
                        "</a>" +
                        "<hr>";
                }
                //este condicional define cualquier ruta local que no contenga el path movie ni el profileView
                else if(path.indexOf("public") > 0 && path!='/foroPelis/public/' && path.indexOf("profileView") < 0 && path.indexOf("movie") < 0 && path.indexOf("selectionMovies") < 0){
                    html_text+= "<a class='linked' href='/foroPelis/public/movie/"+data[i].id+"'>" +
                        "<div class='row align-items-center no-gutters text-dark'>" +
                        "<div class='col-5 ml-1 mt-3'>" +
                        "<div class='photo-result-movie'><figure><img src='../../foroPelis/public/"+data[i].photo+"'></figure></div>" +
                        "</div>" +
                        "<div class='col ml-1'>" +
                        "<div class='row mx-0 justify-content-center align-items-center'>" +
                        "<div class='col-12'>Género: "+data[i].genre+"</div> " +
                        "<div class='col-12'>" +
                        "<div class='name-result-movie font-weight-bold py-2'>"+data[i].name+" ("+data[i].year+")</div>" +
                        "</div>" +
                        "<div class='col-12'>" +
                        "<div class='distribution'><i>"+data[i].distribution+"</i></div>" +
                        "</div>" +
                        "</div>" +
                        "</div>" +
                        "</div>" +
                        "</a>" +
                        "<hr>";
                }else if(path.indexOf("movie") > 0 && hostname!="localhost"){
                    html_text+= "<a class='linked' href='/foroPelis/public/movie/"+data[i].id+"'>" +
                        "<div class='row align-items-center no-gutters text-dark'>" +
                        "<div class='col-5 ml-1 mt-3'>" +
                        "<div class='photo-result-movie'><figure><img src='../../../"+data[i].photo+"'></figure></div>" +
                        "</div>" +
                        "<div class='col ml-1'>" +
                        "<div class='row mx-0 justify-content-center align-items-center'>" +
                        "<div class='col-12'>Género: "+data[i].genre+"</div> " +
                        "<div class='col-12'>" +
                        "<div class='name-result-movie font-weight-bold py-2'>"+data[i].name+" ("+data[i].year+")</div>" +
                        "</div>" +
                        "<div class='col-12'>" +
                        "<div class='distribution'><i>"+data[i].distribution+"</i></div>" +
                        "</div>" +
                        "</div>" +
                        "</div>" +
                        "</div>" +
                        "</a>" +
                        "<hr>";
                }else{
                    html_text+= "<a class='linked' href='/foroPelis/public/movie/"+data[i].id+"'>" +
                        "<div class='row align-items-center no-gutters text-dark'>" +
                        "<div class='col-5 ml-1 mt-3'>" +
                        "<div class='photo-result-movie'><figure><img src='../"+data[i].photo+"'></figure></div>" +
                        "</div>" +
                        "<div class='col ml-1'>" +
                        "<div class='row mx-0 justify-content-center align-items-center'>" +
                        "<div class='col-12'>Género: "+data[i].genre+"</div> " +
                        "<div class='col-12'>" +
                        "<div class='name-result-movie font-weight-bold py-2'>"+data[i].name+" ("+data[i].year+")</div>" +
                        "</div>" +
                        "<div class='col-12'>" +
                        "<div class='distribution'><i>"+data[i].distribution+"</i></div>" +
                        "</div>" +
                        "</div>" +
                        "</div>" +
                        "</div>" +
                        "</a>" +
                        "<hr>";
                }
            }
        }
        return html_text;
    }
});
