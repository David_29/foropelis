<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\MovieController;
use App\Models\Movie;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Ruta index
/*Route::get('/', function () {
    $moviesAllCategories=Movie::all();
    return view('home/index')->with('moviesAllCategories',$moviesAllCategories);
});*/

Auth::routes();

//Ruta de inicio
Route::get('/', [App\Http\Controllers\MovieController::class, 'sendMovies']);


//Ruta de login
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
//Rutas adición película
Route::get('/addMovie', [App\Http\Controllers\MovieController::class, 'index']);
Route::post('/addMovie', [App\Http\Controllers\MovieController::class, 'receiveForm']);
//Rutas edición película
Route::get('/editMovie/{id}', [App\Http\Controllers\MovieController::class, 'editMovieView']);
Route::post('/editMovie/{id}', [App\Http\Controllers\MovieController::class, 'editMovieForm']);
Route::get('/removeMovie/{id}', [App\Http\Controllers\MovieController::class, 'removeMovie']);
//Rutas vista película
Route::get('/movie/{id}', [App\Http\Controllers\MovieController::class, 'movieView']);
Route::post('/movie/{id}', [App\Http\Controllers\MovieController::class, 'receiveComment']);
Route::get('/movie/delete_comment/{id}/{id_comment}', [App\Http\Controllers\MovieController::class, 'deleteComment']);
Route::get('/selectionMovies/{cat}', [App\Http\Controllers\MovieController::class, 'sendMoviesSelection']);
//Rutas del perfil
Route::get('/profileView/{id}', [App\Http\Controllers\MovieController::class, 'profileView']);
Route::post('/profileView/{id}', [App\Http\Controllers\MovieController::class, 'receiveEditProfileForm']);
Route::get('/profileViewVisiting/{id}', [App\Http\Controllers\MovieController::class, 'visitingProfile']);
//Ruta del buscador
Route::get('/buscador', [App\Http\Controllers\SearchController::class, 'searching']);
//Rutas para el buscador en local
Route::get('/foroPelis/public/buscador', [App\Http\Controllers\SearchController::class, 'searching']);
Route::get('/foroPelis/public/movie/{id}', [App\Http\Controllers\MovieController::class, 'movieView']);
