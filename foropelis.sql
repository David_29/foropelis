-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 02-06-2021 a las 17:35:04
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `foropelis`
--
CREATE DATABASE IF NOT EXISTS `foropelis` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `foropelis`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comments`
--

CREATE TABLE `comments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_user` bigint(20) UNSIGNED NOT NULL,
  `id_movie` bigint(20) UNSIGNED NOT NULL,
  `date` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment` varchar(5000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `valoration_comment` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `comments`
--

INSERT INTO `comments` (`id`, `id_user`, `id_movie`, `date`, `comment`, `valoration_comment`, `created_at`, `updated_at`) VALUES
(53, 1, 2, '18-05-2021 16:51:07', 'Increíble e irrepetible.', 5, '2021-05-18 12:51:07', '2021-05-18 12:51:07'),
(56, 6, 17, '02-06-2021 09:52:54', 'Gran película bélica, al ser dirigida por Mel Gibson me esperaba algo menos profundo.', 4, '2021-06-02 05:52:54', '2021-06-02 05:52:54'),
(57, 6, 10, '02-06-2021 09:53:31', 'Maldita sea, vaya engendro aniquilador crearon en esta peli.', 3, '2021-06-02 05:53:31', '2021-06-02 05:53:31'),
(58, 1, 10, '02-06-2021 09:54:12', 'Gran película, me encanta el cine de ciencia ficción.', 5, '2021-06-02 05:54:12', '2021-06-02 05:54:12'),
(59, 2, 9, '02-06-2021 09:55:22', 'La verdad es que para lo que se ha hablado de ella no me ha gustado demasiado.', 1, '2021-06-02 05:55:22', '2021-06-02 05:55:22');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(21, '2014_10_12_000000_create_users_table', 1),
(22, '2014_10_12_100000_create_password_resets_table', 1),
(23, '2019_08_19_000000_create_failed_jobs_table', 1),
(24, '2021_03_24_083316_create_movies_table', 1),
(25, '2021_03_24_093946_create_comments_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `movies`
--

CREATE TABLE `movies` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `genre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `movie_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `synopsis` varchar(5000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `duration` int(11) NOT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `year` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `distribution` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `number_of_evaluations` int(11) DEFAULT NULL,
  `total_evaluations` int(11) DEFAULT NULL,
  `evaluation` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `movies`
--

INSERT INTO `movies` (`id`, `name`, `genre`, `movie_image`, `synopsis`, `duration`, `country`, `year`, `distribution`, `number_of_evaluations`, `total_evaluations`, `evaluation`, `created_at`, `updated_at`) VALUES
(1, 'El padrino', 'Clásicos', 'uploads/imgs/1621351646.jpg', 'América, años 40. Don Vito Corleone (Marlon Brando) es el respetado y temido jefe de una de las cinco familias de la mafia de Nueva York. Tiene cuatro hijos: Connie (Talia Shire), el impulsivo Sonny (James Caan), el pusilánime Fredo (John Cazale) y Michael (Al Pacino), que no quiere saber nada de los negocios de su padre. Cuando Corleone, en contra de los consejos de \'Il consigliere\' Tom Hagen (Robert Duvall), se niega a participar en el negocio de las drogas, el jefe de otra banda ordena su asesinato. Empieza entonces una violenta y cruenta guerra entre las familias mafiosas.', 175, 'Estados Unidos', '1972', 'Marlon Brando, Al Pacino, James Caan, Robert Duvall, Diane Keaton, John Cazale, Talia Shire', 0, 0, 0, '2021-04-17 08:16:46', '2021-05-31 08:55:32'),
(2, 'Blade Runner', 'Clásicos', 'uploads/imgs/1618654658.jpg', 'Noviembre de 2019. A principios del siglo XXI, la poderosa Tyrell Corporation creó, gracias a los avances de la ingeniería genética, un robot llamado Nexus 6, un ser virtualmente idéntico al hombre pero superior a él en fuerza y agilidad, al que se dio el nombre de Replicante. Estos robots trabajaban como esclavos en las colonias exteriores de la Tierra. Después de la sangrienta rebelión de un equipo de Nexus-6, los Replicantes fueron desterrados de la Tierra. Brigadas especiales de policía, los Blade Runners, tenían órdenes de matar a todos los que no hubieran acatado la condena. Pero a esto no se le llamaba ejecución, se le llamaba \"retiro\". Tras un grave incidente, el ex Blade Runner Rick Deckard es llamado de nuevo al servicio para encontrar y \"retirar\" a unos replicantes rebeldes.', 117, 'Estados Unidos', '1982', 'Harrison Ford, Rutger Hauer, Sean Young, Daryl Hannah, Edward James Olmos', 5, 1, 5, '2021-04-17 08:17:38', '2021-05-18 12:51:07'),
(3, 'Salvar al soldado Ryan', 'Bélico', 'uploads/imgs/1621351497.jpg', 'Segunda Guerra Mundial, 1944. Tras el desembarco de los Aliados en Normandía, a un grupo de soldados americanos se le encomienda una peligrosa misión: poner a salvo al soldado James Ryan. Los hombres de la patrulla del capitán John Miller deben arriesgar sus vidas para encontrar a este soldado, cuyos tres hermanos han muerto en la guerra. Lo único que se sabe del soldado Ryan es que se lanzó con su escuadrón de paracaidistas detrás de las líneas enemigas.', 170, 'Estados Unidos', '1998', 'Tom Hanks, Tom Sizemore, Edward Burns, Matt Damon, Barry Pepper, Giovanni Ribisi, Adam Goldberg', 0, 0, 0, '2021-04-17 08:16:46', '2021-05-18 13:24:57'),
(4, 'Apocalypse Now', 'Clásicos', 'uploads/imgs/1620837553.jpg', 'Durante la guerra de Vietnam, al joven Capitán Willard, un oficial de los servicios de inteligencia del ejército estadounidense, se le ha encomendado entrar en Camboya con la peligrosa misión de eliminar a Kurtz, un coronel renegado que se ha vuelto loco. El capitán deberá ir navegar por el río hasta el corazón de la selva, donde parece ser que Kurtz reina como un buda despótico sobre los miembros de la tribu Montagnard, que le adoran como a un dios.', 153, 'Estados Unidos', '1979', 'Martin Sheen, Marlon Brando, Robert Duvall, Frederic Forrest, Sam Bottoms, Albert Hall', 0, 0, 0, '2021-04-17 08:17:38', '2021-05-14 09:20:05'),
(6, 'Mortal Combat', 'Acción', 'uploads/imgs/1620036117.jpg', 'En “Mortal Kombat”, Cole Young, el luchador de MMA (Artes Marciales Mixtas), acostumbrado a recibir palizas por dinero, desconoce su ascendencia, y tampoco sabe por qué el emperador Shang Tsung de Outworld ha enviado a su mejor guerrero, Sub-Zero, un Cryomancer sobrenatural, para dar caza a Cole. Cole teme por la seguridad de su familia y busca a Sonya Blade siguiendo las indicaciones de Jax, un comandante de las Fuerzas Especiales que tiene la misma extraña marca de dragón con la que nació Cole. No tarda en llegar al templo de Lord Raiden, un Dios Anciano y el protector de Earthrealm, que ofrece refugio a los que llevan la marca. Aquí, Cole entrena con los experimentados guerreros Liu Kang, Kung Lao y el mercenario rebelde Kano, mientras se prepara para enfrentarse a los mayores campeones de la Tierra contra los enemigos de Outworld (El Mundo Exterior) en una batalla de enorme envergadura por el universo. Veremos si los esfuerzos de Cole se ven recompensados y consigue desbloquear su arcana -ese inmenso poder que surge del interior de su alma— a tiempo de salvar no solo a su familia, sino para detener Outworld de una vez por todas.', 110, 'Estados Unidos', '2021', 'Lewis Tan, Joe Taslim, Jessica McNamee, Josh Lawson, Mehcad Brooks, Tadanobu Asano', 0, 0, 0, '2021-04-17 08:17:38', '2021-05-18 13:30:03'),
(7, 'Braveheart', 'Clásicos', 'uploads/imgs/1618654606.jpg', 'En el siglo XIV, los escoceses viven oprimidos por los gravosos tributos y las injustas leyes impuestas por los ingleses. William Wallace es un joven escocés que regresa a su tierra despues de muchos años de ausencia. Siendo un niño, toda su familia fue asesinada por los ingleses, razón por la cual se fue a vivir lejos con un tío suyo.', 177, 'Estados Unidos', '1995', 'Mel Gibson, Sophie Marceau, Patrick McGoohan, Angus MacFadyen, Catherine McCormack', 0, 0, 0, '2021-04-17 08:16:46', '2021-04-17 08:16:46'),
(9, 'Pretty Woman', 'Romántico', 'uploads/imgs/1621343834.jpg', 'Edward Lewis (Richard Gere), un apuesto y rico hombre de negocios, contrata a una prostituta, Vivian Ward (Julia Roberts), durante un viaje a Los Angeles. Tras pasar con ella la primera noche, Edward le ofrece dinero a Vivian para que pase con él toda la semana y le acompañe a diversos actos sociales.', 119, 'Estados Unidos', '1990', 'Julia Roberts, Richard Gere, Hector Elizondo, Jason Alexander, Ralph Bellamy, Laura San Giacomo', 1, 1, 1, '2021-05-18 11:17:14', '2021-06-02 12:51:40'),
(10, 'Life', 'Ciencia ficción', 'uploads/imgs/1621348981.jpg', 'Seis miembros de la tripulación de la Estación Espacial Internacional están a punto de lograr uno de los descubrimientos más importantes en la historia humana: la primera evidencia de vida extraterrestre en Marte. A medida que el equipo comienza a investigar y sus métodos tienen consecuencias inesperadas, la forma viviente demostrará ser más inteligente de lo que cualquiera esperaba.', 103, 'Estados Unidos', '2017', 'Jake Gyllenhaal, Rebecca Ferguson, Ryan Reynolds, Hiroyuki Sanada, Ariyon Bakare, Olga Dykhovichnaya', 8, 2, 4, '2021-05-18 12:43:01', '2021-06-02 05:54:12'),
(11, 'Intocable', 'Drama', 'uploads/imgs/1621351927.jpg', 'Philippe, un aristócrata millonario que se ha quedado tetrapléjico a causa de un accidente de parapente, contrata como cuidador a domicilio a Driss, un inmigrante de un barrio marginal recién salido de la cárcel. Aunque, a primera vista, no parece la persona más indicada, los dos acaban logrando que convivan Vivaldi y Earth Wind and Fire, la elocuencia y la hilaridad, los trajes de etiqueta y el chándal. Dos mundos enfrentados que, poco a poco, congenian hasta forjar una amistad tan disparatada, divertida y sólida como inesperada, una relación única en su especie de la que saltan chispas.', 109, 'Francia', '2011', 'François Cluzet, Omar Sy, Anne Le Ny, Audrey Fleurot, Clotilde Mollet, Joséphine de Meaux', NULL, NULL, NULL, '2021-05-18 13:32:07', '2021-05-18 13:32:07'),
(12, 'Cadena perpetua', 'Drama', 'uploads/imgs/1621353368.jpg', 'Acusado del asesinato de su mujer, Andrew Dufresne (Tim Robbins), tras ser condenado a cadena perpetua, es enviado a la cárcel de Shawshank. Con el paso de los años conseguirá ganarse la confianza del director del centro y el respeto de sus compañeros de prisión, especialmente de Red (Morgan Freeman), el jefe de la mafia de los sobornos.', 142, 'Estados Unidos', '1994', 'Tim Robbins, Morgan Freeman, Bob Gunton, James Whitmore, Gil Bellows, William Sadler', NULL, NULL, NULL, '2021-05-18 13:56:09', '2021-05-18 13:56:09'),
(13, 'Zoolander', 'Comedia', 'uploads/imgs/1621353522.jpg', 'Derek Zoolander (Stiller) ha sido el modelo masculino más cotizado durante los últimos tres años. La noche de la gala que podría suponer su cuarta corona, el galardón se lo lleva un nuevo modelo llamado Hansel (Wilson). Derek queda en entredicho y como un idiota, y decide retirarse. Sin embargo, un prestigioso diseñador le pide que desfile para él.', 89, 'Estados Unidos', '2001', 'Ben Stiller, Owen Wilson, Milla Jovovich, Will Ferrell, Christine Taylor, David Duchovny', NULL, NULL, NULL, '2021-05-18 13:58:42', '2021-05-18 13:58:42'),
(14, 'Mejor solo que mal acompañado', 'Comedia', 'uploads/imgs/1621353659.jpg', 'Neal Page (Steve Martin) es un ejecutivo de publicidad que quiere llegar a Chicago a tiempo para pasar el dia de Accion De Gracias con su familia. Pero su vida se convierte en una pesadilla cuando tropieza con Del Griffith (John Candy), un vendedor de cortinas de baño bocazas e insoportable, del que no podrá librarse durante un accidentado viaje.', 100, 'Estados Unidos', '1987', 'Steve Martin, John Candy, Laila Robins, Lyman Ward, Michael McKean, Dylan Baker', NULL, NULL, NULL, '2021-05-18 14:00:59', '2021-05-31 08:55:18'),
(15, 'Expediente Warren: The Conjuring', 'Terror', 'uploads/imgs/1621354370.jpg', 'Basada en una historia real documentada por los reputados demonólogos Ed y Lorraine Warren. Narra los encuentros sobrenaturales que vivió la familia Perron en su casa de Rhode Island a principios de los 70. El matrimonio Warren, investigadores de renombre en el mundo de los fenómenos paranormales, acudieron a la llamada de esta familia aterrorizada por la presencia en su granja de un ser maligno.', 112, 'Estados Unidos', '2013', 'Lili Taylor, Vera Farmiga, Patrick Wilson, Joey King, Ron Livingston, Mackenzie Foy, Shanley Caswell', NULL, NULL, NULL, '2021-05-18 14:12:50', '2021-05-18 14:12:50'),
(16, 'El resplandor', 'Terror', 'uploads/imgs/1621354922.jpg', 'Jack Torrance se traslada con su mujer y su hijo de siete años al impresionante hotel Overlook, en Colorado, para encargarse del mantenimiento de las instalaciones durante la temporada invernal, época en la que permanece cerrado y aislado por la nieve. Su objetivo es encontrar paz y sosiego para escribir una novela. Sin embargo, poco después de su llegada al hotel, al mismo tiempo que Jack empieza a padecer inquietantes trastornos de personalidad, se suceden extraños y espeluznantes fenómenos paranormales.', 146, 'Reino Unido', '1980', 'Jack Nicholson, Shelley Duvall, Danny Lloyd, Scatman Crothers, Barry Nelson, Philip Stone', NULL, NULL, NULL, '2021-05-18 14:22:02', '2021-05-18 14:22:02'),
(17, 'Hasta el último hombre', 'Bélico', 'uploads/imgs/1621355039.jpg', 'Narra la historia de Desmond Doss, un joven médico militar que participó en la sangrienta batalla de Okinawa, en el Pacífico durante la II Guerra Mundial, y se convirtió en el primer objetor de conciencia en la historia estadounidense en recibir la Medalla de Honor del Congreso. Doss quería servir a su país, pero desde pequeño se había hecho una promesa a sí mismo: no coger jamás ningún arma.', 131, 'Estados Unidos', '2016', 'Andrew Garfield, Sam Worthington, Hugo Weaving, Vince Vaughn, Teresa Palmer, Luke Bracey', 4, 1, 4, '2021-05-18 14:23:59', '2021-06-02 05:52:54'),
(18, 'Moulin Rouge', 'Romántico', 'uploads/imgs/1621355140.jpg', 'Ambientada en el París bohemio de 1900. Satine, la estrella más rutilante del Moulin Rouge, encandila a toda la ciudad con sus bailes llenos de sensualidad y su enorme belleza. Atrapada entre el amor de dos hombres, un joven escritor y un duque, lucha por hacer realidad su sueño de convertirse en actriz. Pero, en un mundo en el que todo vale, excepto enamorarse, nada es fácil.', 125, 'Australia', '2001', 'Nicole Kidman, Ewan McGregor, John Leguizamo, David Wenham, Jim Broadbent, Richard Roxburgh', NULL, NULL, NULL, '2021-05-18 14:25:40', '2021-05-18 14:25:40'),
(19, 'The equalizer', 'Acción', 'uploads/imgs/1621355232.jpg', 'Robert McCall, un antiguo agente de la CIA que lleva ahora una vida tranquila, abandona su retiro para ayudar a Teri, una joven prostituta que está siendo explotada por la mafia rusa. A pesar de que aseguró no volver a ser violento, contemplar tanta crueldad despertará en Robert un implacable y renovado deseo de justicia... Versión cinematográfica de la serie de televisión de los 80, \'El justiciero\'.', 128, 'Estados Unidos', '2014', 'Denzel Washington, Marton Csokas, Chloë Grace Moretz, David Harbour, Melissa Leo', NULL, NULL, NULL, '2021-05-18 14:27:12', '2021-05-18 14:27:12'),
(20, 'Interstellar', 'Ciencia ficción', 'uploads/imgs/1621355480.jpg', 'Al ver que la vida en la Tierra está llegando a su fin, un grupo de exploradores dirigidos por el piloto Cooper (McConaughey) y la científica Amelia (Hathaway) emprende una misión que puede ser la más importante de la historia de la humanidad: viajar más allá de nuestra galaxia para descubrir algún planeta en otra que pueda garantizar el futuro de la raza humana.', 169, 'Estados Unidos', '2014', 'Matthew McConaughey, Anne Hathaway, David Gyasi, Jessica Chastain, Mackenzie Foy', NULL, NULL, NULL, '2021-05-18 14:31:20', '2021-05-18 14:31:20');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bio` varchar(5000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fav_movie` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `role`, `user_image`, `bio`, `fav_movie`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'David', 'd290397@hotmail.es', NULL, '$2y$10$GPAyTnKSMRzUvdAajsmHAeP6hot9YsHjJsGjfPUwnoNrgO6L8JmzC', 'Administrador', '1618654193.jpg', 'Soy el admin.', NULL, NULL, '2021-04-17 08:09:53', '2021-06-02 09:37:56'),
(2, 'User 2', 'user2@hotmail.es', NULL, '$2y$10$oKp2E7sGuI9pgSK6dAwDd.xNgch8QOx63shRuhGfFR3Qv8u4CeskW', 'Estándar', '1620985556.jpg', 'Hola que tal, me gusta el cine de acción. Es...apasionante.', NULL, NULL, '2021-04-28 08:43:13', '2021-06-02 05:52:01'),
(6, 'user 3', 'user3@hotmail.es', NULL, '$2y$10$./ruanWEhLLkKPUon2l/k.PLrkw.2uSzYlruBewxOdzBu4frMGwNC', 'Estándar', '1620833802.jpg', 'Buenas, vengo aquí a ver que se cuece.', NULL, NULL, '2021-05-12 13:36:42', '2021-06-02 05:52:25'),
(9, 'user 4', 'user4@hotmail.es', NULL, '$2y$10$mIMW5uXWDPHcltk9KOldJelb.GIqsEmNNCobx8.G3CKcyEHCi8U6G', 'Estándar', '1620837651.jpg', '¡Bienvenidos compañeros!', NULL, NULL, '2021-05-12 14:27:41', '2021-05-12 14:40:51'),
(10, 'Usuario Profe', 'profe@hotmail.com', NULL, '$2y$10$rgV9LHqmCzty0g5seEImMewQbIENZWAdxMjpueqFkslBNVaV0BC/O', 'Administrador', '1622620745.jpg', 'Soy el profesor.', NULL, NULL, '2021-06-02 05:59:05', '2021-06-02 05:59:05');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comments_id_user_foreign` (`id_user`),
  ADD KEY `comments_id_movie_foreign` (`id_movie`);

--
-- Indices de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `movies`
--
ALTER TABLE `movies`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `comments`
--
ALTER TABLE `comments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT de la tabla `movies`
--
ALTER TABLE `movies`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_id_movie_foreign` FOREIGN KEY (`id_movie`) REFERENCES `movies` (`id`),
  ADD CONSTRAINT `comments_id_user_foreign` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
