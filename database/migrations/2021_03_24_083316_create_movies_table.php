<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMoviesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movies', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('genre');
            $table->string('movie_image');
            $table->string('synopsis',5000);
            $table->integer('duration');
            $table->string('country');
            $table->string('year');
            $table->string('distribution')->nullable();
            $table->integer('number_of_evaluations')->nullable();
            $table->integer('total_evaluations')->nullable();
            $table->integer('evaluation')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movies');
    }
}
